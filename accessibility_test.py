import json
from selenium import webdriver
from axe_selenium_python import Axe
import time
import os

# Instancier le navigateur WebDriver (dans cet exemple, Chrome)
driver = webdriver.Chrome()

# Ouvrir la page web à tester
driver.get("index.html")

# Attendre un moment pour que la page se charge complètement
time.sleep(2)

# Instancier l'objet Axe avec le navigateur WebDriver
axe = Axe(driver)
# Injecter le script Axe dans la page web
axe.inject()

# Exécuter les tests d'accessibilité
results = axe.run()

# Enregistrer les résultats des tests au format JSON
report_path = 'axe-reports/accessibility_report.json'
os.makedirs(os.path.dirname(report_path), exist_ok=True)
with open(report_path, 'w') as report_file:
    json.dump(results, report_file, indent=2)

# Afficher les résultats des tests
if len(results["violations"]) == 0:
    print("Aucun problème d'accessibilité détecté !")
else:
    print("Problèmes d'accessibilité détectés :")
    for violation in results["violations"]:
        print("- Description :", violation["description"])
        print("- Impact :", violation["impact"])
        print("- Aide à la correction :", violation["help"])
        print("- Liste des éléments affectés :", violation["nodes"])
        print("-" * 50)

# Fermer le navigateur
driver.quit()
